using System.Text.Json.Serialization;

namespace BizCap.Yago.Commons.Enumerateds {
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum IngredientTypes {
        GRAPE,
        HOP,
        MALT,
        COLA_SYRUP,
        SUGAR,
    }
}