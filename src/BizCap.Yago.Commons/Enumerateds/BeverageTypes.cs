using System.Text.Json.Serialization;

namespace BizCap.Yago.Commons.Enumerateds
{  
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum BeverageTypes
    {
        COCA_COLA,
        SPRITE,
        STOUT,
        WEISS,
        PALE_ALE,
        BOCK,
        PILSEN,
        RED_WINE,
        WHITE_WHINE,
        SCOTCH,
        BOURBON,
    }
}