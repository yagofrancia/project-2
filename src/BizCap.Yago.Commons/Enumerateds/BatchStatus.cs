using System.Text.Json.Serialization;

namespace BizCap.Yago.Commons.Enumerateds
{
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum BatchStatus
    {
        IN_PROGRESS,
        FINISHED,
        FAILED
    }
}