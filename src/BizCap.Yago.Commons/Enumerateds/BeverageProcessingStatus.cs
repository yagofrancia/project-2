using System.Text.Json.Serialization;

namespace BizCap.Yago.Commons.Enumerateds
{
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum BeverageProcessingStatus
    {
        NOT_STARTED,
        MOAGEM,
        SEPARACAO,
        FILTRAGEM,
        FERMENTACAO,
        CITRIFICACAO
    }
}