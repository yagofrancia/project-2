using System;

namespace BizCap.Yago.Commons.Middlewares {
    public class Middleware<T> {
        public Predicate<T> Approved { get; set; }
        public Func<T, T> Callback { get; set; }
    }
}