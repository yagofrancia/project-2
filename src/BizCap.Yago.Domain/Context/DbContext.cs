using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Bson.Serialization.IdGenerators;
using MongoDB.Driver;

namespace BizCap.Yago.Domain.Models
{
    public class DbContext
    {
        public IMongoDatabase Database { get; set; }

        public DbContext(IMongoDatabase database)
        {
            Database = database;
        }

        public IMongoCollection<Beverage> Beverages => Database.GetCollection<Beverage>("Beverages");

        public IMongoCollection<Ingredient> Ingredients
        {
            get
            {
                return Database.GetCollection<Ingredient>("Ingredients");
            }
        }

        public IMongoCollection<Batch> Batches
        {
            get
            {
                return Database.GetCollection<Batch>("Batches");
            }
        }
    }
}