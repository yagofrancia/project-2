using BizCap.Yago.Domain.Repositories;
using BizCap.Yago.Domain.Repositories.Implementations;
using Microsoft.Extensions.DependencyInjection;

namespace BizCap.Yago.Domain.Extensions
{
    public static class RepositoryExtensions
    {
        public static IServiceCollection AddRepositories(this IServiceCollection service)
        {
            return service
                .AddSingleton<IBeverageRepository, BeverageRepository>()
                .AddSingleton<IIngredientRepository, IngredientRepository>();
        }
    }
}