using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using BizCap.Yago.Domain.Models;
using MongoDB.Driver;

namespace BizCap.Yago.Domain.Extensions
{
    public static class DbExtensions
    {
        public static IServiceCollection AddDatabase(this IServiceCollection service, IConfiguration configuration)
        {
            return service.AddSingleton<DbContext>(sp =>
            {
                var settings = MongoClientSettings.FromUrl(new MongoUrl(configuration.GetConnectionString("DatabaseServer")));
                var client = new MongoClient(settings);

                return new DbContext(
                    client.GetDatabase(configuration.GetConnectionString("DatabaseName"))
                );
            }
            );
        }
    }
}