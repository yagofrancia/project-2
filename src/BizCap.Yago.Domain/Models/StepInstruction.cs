namespace BizCap.Yago.Domain.Models
{
    public class StepInstruction
    {
        public string Name { get; set; }
    }
}