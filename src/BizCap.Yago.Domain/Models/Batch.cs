using System;
using BizCap.Yago.Commons.Enumerateds;

namespace BizCap.Yago.Domain.Models
{
    public class Batch : Entity
    {
        public BeverageTypes Type { get; set; }
        public int Capacity { get; set; }
        public int Quantity { get; set; }
        public DateTime StartedAssemblyAt { get; set; }
        public DateTime FinishedAt { get; set; }
        public BatchStatus Status { get; set; }
    }
}