using System.Collections.Generic;
using BizCap.Yago.Commons.Enumerateds;
namespace BizCap.Yago.Domain.Models
{
    public class WeissBeer : Beer
    {
        public override BeverageTypes Type
        {
            get => BeverageTypes.WEISS;
        }
    }
}