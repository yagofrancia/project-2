using System.Text.Json.Serialization;
using BizCap.Yago.Commons.Enumerateds;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace BizCap.Yago.Domain.Models
{
    public class IngredientRequirement
    {
        [BsonRepresentation(BsonType.String)]
        public IngredientTypes Type { get; set; }
        public int Quantity { get; set; }
    }
}