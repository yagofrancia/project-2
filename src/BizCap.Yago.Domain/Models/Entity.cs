using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace BizCap.Yago.Domain.Models {
    public class Entity {

        [BsonId]
        [BsonRepresentation(BsonType.String)]
        public Guid Id { get; set; }
    }
}