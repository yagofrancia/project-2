using System.Collections.Generic;
using BizCap.Yago.Commons.Enumerateds;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace BizCap.Yago.Domain.Models
{
    public abstract class Beverage : Entity
    {
        [BsonIgnore]
        public BeverageProcessingStatus Status { get; set; }
        [BsonRepresentation(BsonType.String)]
        public abstract BeverageTypes Type { get; }
        public IEnumerable<StepInstruction> ProcessingSteps { get; set; }
        public IEnumerable<IngredientRequirement> Ingredients { get; set; }

    }
}