using BizCap.Yago.Commons.Enumerateds;

namespace BizCap.Yago.Domain.Models
{
    public class Belt : Entity
    {
        public BeverageTypes Type { get; set; }
        public int MyProperty { get; set; }
    }
}