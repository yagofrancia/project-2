using BizCap.Yago.Commons.Enumerateds;
using BizCap.Yago.Domain.Models;
using System.Threading.Tasks;

namespace BizCap.Yago.Domain.Repositories
{
    public interface IIngredientRepository : IRepository<Ingredient>
    {
        Task<Ingredient> FindOneByTypeAsync(IngredientTypes type);
    }
}