using BizCap.Yago.Commons.Enumerateds;
using BizCap.Yago.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BizCap.Yago.Domain.Repositories
{
    public interface IBatchRepository : IRepository<Batch>
    {
        Task<IEnumerable<Batch>> GetCollectionByType(BeverageTypes type);
    }
}