using System;
using System.Threading.Tasks;
using BizCap.Yago.Domain.Models;
using MongoDB.Driver;
using MongoDB.Driver.Linq;

namespace BizCap.Yago.Domain.Repositories
{
    public abstract class Repository<T> : IRepository<T> where T : Entity
    {

        abstract public IMongoCollection<T> GetCollection();

        public async Task<T> FindOneByIdAsync(Guid id) =>
            await GetCollection()
            .AsQueryable()
            .SingleOrDefaultAsync(c => c.Id == id);

        public async Task InsertOneAsync(T entity)
        {
            await GetCollection()
                .InsertOneAsync(entity);
        }

        public async Task UpdateOneAsync(T entity)
        {
            await GetCollection()
                .ReplaceOneAsync(e => e.Id == entity.Id, entity);
        }

        public async Task UpsertOneAsync(T entity)
        {
            await GetCollection()
                .ReplaceOneAsync(e => e.Id == entity.Id, entity, new ReplaceOptions { IsUpsert = true });
        }
    }
}