using System.Threading.Tasks;
using BizCap.Yago.Commons.Enumerateds;
using BizCap.Yago.Domain.Models;

namespace BizCap.Yago.Domain.Repositories
{
    public interface IBeverageRepository : IRepository<Beverage>
    {
        Task<Beverage> FindByTypeAsync(BeverageTypes type);
    }
}