using System;
using System.Threading.Tasks;
using BizCap.Yago.Commons.Enumerateds;
using BizCap.Yago.Domain.Models;
using MongoDB.Driver;
using System.Linq;
using MongoDB.Driver.Linq;

namespace BizCap.Yago.Domain.Repositories.Implementations
{
    public class BeverageRepository : Repository<Beverage>, IBeverageRepository
    {
        private readonly DbContext _context;

        public BeverageRepository(DbContext context)
        {
            _context = context;
        }

        public override IMongoCollection<Beverage> GetCollection()
        {
            return _context.Beverages;
        }
        public async Task<Beverage> FindByTypeAsync(BeverageTypes type)
        {
            return await GetCollection()
                            .AsQueryable()
                            .FirstOrDefaultAsync(b => b.Type == type);
        }
    }

}