using System.Threading.Tasks;
using BizCap.Yago.Commons.Enumerateds;
using BizCap.Yago.Domain.Models;
using MongoDB.Driver;
using MongoDB.Driver.Linq;

namespace BizCap.Yago.Domain.Repositories
{
    public class IngredientRepository : Repository<Ingredient>, IIngredientRepository
    {
        private readonly DbContext _context;

        public IngredientRepository(DbContext context)
        {
            _context = context;
        }

        public override IMongoCollection<Ingredient> GetCollection()
        {
            return _context.Ingredients;
        }

        public async Task<Ingredient> FindOneByTypeAsync(IngredientTypes type)
        {
            return await GetCollection()
                            .AsQueryable()
                            .FirstOrDefaultAsync(i => i.Type == type);
        }
    }
}