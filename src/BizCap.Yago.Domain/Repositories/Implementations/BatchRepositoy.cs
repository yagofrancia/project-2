using System.Threading.Tasks;
using BizCap.Yago.Commons.Enumerateds;
using BizCap.Yago.Domain.Models;
using MongoDB.Driver;
using System.Linq;
using MongoDB.Driver.Linq;
using System.Collections.Generic;

namespace BizCap.Yago.Domain.Repositories.Implementations
{
    public class BatchRepository : Repository<Batch>, IBatchRepository
    {
        private readonly DbContext _context;

        public BatchRepository(DbContext context)
        {
            _context = context;
        }

        public override IMongoCollection<Batch> GetCollection()
        {
            return _context.Batches;
        }

        public async Task<IEnumerable<Batch>> GetCollectionByType(BeverageTypes type)
        {
            return await GetCollection()
                            .AsQueryable()
                            .Where(b => b.Type == type)
                            .ToListAsync();
        }
    }

}