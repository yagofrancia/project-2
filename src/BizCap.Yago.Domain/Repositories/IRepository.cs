using System;
using System.Threading.Tasks;
using BizCap.Yago.Domain.Models;
using MongoDB.Driver;

namespace BizCap.Yago.Domain.Repositories
{
    // essa sintaxe where T : Base tem a funcao de restringir o tipo genérico a ser passado
    public interface IRepository<T> where T : Entity
    {
        public IMongoCollection<T> GetCollection();
        public Task<T> FindOneByIdAsync(Guid id);
        public Task InsertOneAsync(T entity);
        public Task UpdateOneAsync(T entity);
        public Task UpsertOneAsync(T entity);
    }
}