using BizCap.Yago.Commons.Enumerateds;

namespace BizCap.Yago.Api.Requests
{
    public class FabricateBeverageRequest
    {
        public BeverageTypes Type { get; set; }
    }
}