using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Configuration;
using BizCap.Yago.Domain.Extensions;
using BizCap.Yago.Api.GraphQL;
using BizCap.Yago.Processor.Extensions;
using BizCap.Yago.Domain.Models;

namespace BizCap.Yago.Api
{
    public class Startup

    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddBeverageProcessor();
            services.AddProcessingSteps();
            services.AddServices();
            services
                .AddDatabase(Configuration)
                .AddRepositories()
                .AddGraphQLServer()
                .AddQueryType<QueryType>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                // endpoints.MapGraphQL();
                endpoints.MapControllers();
            });
        }
    }
}
