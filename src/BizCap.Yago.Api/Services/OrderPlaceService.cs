using BizCap.Yago.Processor.Queue;
using BizCap.Yago.Processor.Queue.Messages;

namespace BizCap.Yago.Api.Services
{
    public class OrderPlaceService
    {
        private readonly IProducer<BeverageRequestMessage> _producer;

        public OrderPlaceService(IProducer<BeverageRequestMessage> producer)
        {
            _producer = producer;
        }
    }
}