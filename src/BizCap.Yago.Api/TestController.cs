// TO-DO remove this class as soon as testing is finished
using System.Collections.Generic;
using System.Threading.Tasks;
using BizCap.Yago.Commons.Enumerateds;
using BizCap.Yago.Domain.Models;
using BizCap.Yago.Processor.Services;
using Microsoft.AspNetCore.Mvc;

namespace BizCap.Yago.Api
{
    [ApiController]
    [Route("my-controller")]
    public class TestController : ControllerBase
    {
        private readonly BeverageService _beverageService;
        private readonly IngredientService _ingredientService;
        private readonly FabricationService _fabricationService;

        public TestController(BeverageService beverageService, IngredientService ingredientService, FabricationService fabricationService)
        {
            _beverageService = beverageService;
            _ingredientService = ingredientService;
            _fabricationService = fabricationService;
        }

        [HttpGet]
        [Route("create")]
        public async Task<string> Produce()
        {
            var weiss = new WeissBeer
            {
                Ingredients = new List<IngredientRequirement>
                {
                    new IngredientRequirement
                    {
                        Quantity = 1,
                        Type = Commons.Enumerateds.IngredientTypes.MALT
                    }
                },
                ProcessingSteps = new List<StepInstruction>()
                {
                    new StepInstruction
                    {
                        Name = "SeparationStep"
                    }
                },
                Status = BeverageProcessingStatus.NOT_STARTED
            };
            await _beverageService.AddBeverage(weiss);
            return "success";
        }

        [HttpPost]
        [Route("produce")]
        public async Task<string> FabricateBeverage(BeverageTypes type)
        {
            await _fabricationService.Fabricate(type, 20);
            return "success";
        }

        [HttpPost]
        [Route("add-ingredient")]
        public async Task<string> CreateIngredient()
        {
            var ingredient = new Ingredient { Quantity = 400, Type = IngredientTypes.MALT };
            await _ingredientService.UpsertIngredient(ingredient);
            return "success";
        }
    }
}
