using BizCap.Yago.Domain.Models;
using HotChocolate.Types;

namespace BizCap.Yago.Api.GraphQL
{
    public class BeverageType : ObjectType<Beverage>
    {
        protected override void Configure(IObjectTypeDescriptor<Beverage> descriptor)
        {
        }
    }
}