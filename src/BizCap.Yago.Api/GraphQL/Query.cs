using BizCap.Yago.Domain.Models;

namespace BizCap.Yago.Api {
    public class Query {
        public Beverage GetBeverage { get; set; }
    }
}