using BizCap.Yago.Domain.Models;
using HotChocolate.Types;

namespace BizCap.Yago.Api.GraphQL
{
    public class QueryType : ObjectType<Query>
    {
        protected override void Configure(IObjectTypeDescriptor<Query> descriptor)
        {
            descriptor
                .Field(b => b.GetBeverage)
                .Type<BeverageType>()
                .Argument("name", a => a.Type<NonNullType<StringType>>());
                // o tipo generico do metodo ArgumentValue é para definir o tipo que o campo vai entrar no codigo
                // esse tipo precisa ser herdado ou o mesmo tipo generico passado na criacao do argumento
                // .Resolve(ctx => new Beverage { Name = ctx.ArgumentValue<string>("name")});
        }
    }
}