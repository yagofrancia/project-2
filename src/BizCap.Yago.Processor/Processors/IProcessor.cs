using System.Threading.Tasks;

namespace BizCap.Yago.Processor.Processors
{
    public interface IProcessor<T>
    {
        Task<T> ProcessSteps(T entity);
    }
}