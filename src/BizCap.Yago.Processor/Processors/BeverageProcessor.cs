using BizCap.Yago.Domain.Models;
using System;
using BizCap.Yago.Processor.Steps;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;

namespace BizCap.Yago.Processor.Processors
{
    public class BeverageProcessor : IProcessor<Beverage>
    {
        private readonly IServiceProvider _sp;

        public BeverageProcessor(IServiceProvider sp)
        {
            _sp = sp;
        }

        public async Task<Beverage> ProcessSteps(Beverage entity)
        {
            foreach (var stepInstruction in entity.ProcessingSteps)
            {
                var className = stepInstruction.Name;
                var type = Type.GetType($"{className}, BizCap.Yago.Processor");
                var step = (IStep<Beverage>)_sp.GetRequiredService(type);
                entity = (Beverage)await step.Process(entity);
            }
            return entity;
        }

    }

}