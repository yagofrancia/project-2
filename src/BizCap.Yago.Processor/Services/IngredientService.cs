using System.Threading.Tasks;
using BizCap.Yago.Commons.Enumerateds;
using BizCap.Yago.Domain.Repositories;
using System;
using BizCap.Yago.Domain.Models;

namespace BizCap.Yago.Processor.Services
{
    public class IngredientService
    {
        private readonly IIngredientRepository _repository;

        public IngredientService(IIngredientRepository repository)
        {
            _repository = repository;
        }

        public async Task ConsumeIngredient(IngredientTypes type, int requiredQty)
        {
            var ingredient = await _repository.FindOneByTypeAsync(type);
            if (requiredQty > ingredient.Quantity) throw new Exception($"Not enough quantity of {type.ToString()}");

            await SubtractIngredient(ingredient, requiredQty);
        }
        public async Task UpsertIngredient(Ingredient ingredient)
        {
            var inventoryIngredient = await _repository.FindOneByTypeAsync(ingredient.Type);
            if (inventoryIngredient == null) {
                await _repository.InsertOneAsync(ingredient);
            } else {
                inventoryIngredient.Quantity += ingredient.Quantity;
                await _repository.UpdateOneAsync(inventoryIngredient);
            }
        }
        async Task SubtractIngredient(Ingredient ingredient, int requiredQty)
        {
            ingredient.Quantity -= requiredQty;
            await _repository.UpdateOneAsync(ingredient);
        }

    }
}