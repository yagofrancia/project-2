using System.Collections.Generic;
using System.Threading.Tasks;
using BizCap.Yago.Commons.Enumerateds;
using BizCap.Yago.Domain.Models;
using BizCap.Yago.Domain.Repositories;
using System.Linq;

namespace BizCap.Yago.Processor.Services
{
    public class BatchService
    {
        private readonly IBeverageRepository _beverageRepository;
        private readonly IBatchRepository _batchRepository;

        public async Task<IEnumerable<Batch>> GetIncompleteBatchesByType(BeverageTypes type)
        {
            var allBatches = await _batchRepository.GetCollectionByType(type);
            return allBatches.Where(b => b.Quantity < b.Capacity).ToList();
        }

        public async Task CreateBatchAsync(Batch batch)
        {
            await _batchRepository.InsertOneAsync(batch);
        }

        public async Task CompleteBatchAsync(Batch batch)
        {
            await _batchRepository.UpdateOneAsync(batch);
        }
    }
}