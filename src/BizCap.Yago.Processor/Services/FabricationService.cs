using BizCap.Yago.Commons.Enumerateds;
using BizCap.Yago.Domain.Models;
using System;
using System.Threading.Tasks;

namespace BizCap.Yago.Processor.Services
{
    public class FabricationService
    {
        private readonly IngredientService _ingredientService;
        private readonly BeverageService _beverageService;
        private readonly BatchService _batchService;

        public async Task Fabricate(BeverageTypes type, int qty)
        {
            // passar a responsabilidade das operacoes abaixo para o servico de lotes
            var max = 50;
            var beverage = await _beverageService.GetBeverageByTypeAsync(type);
            var incompleteBatches = await _batchService.GetIncompleteBatchesByType(type);
            var totalProduced = 0;
            foreach (var batch in incompleteBatches)
            {
                for (var i = 0; i < (max - batch.Capacity); i += 1)
                {
                    beverage = await _beverageService.Process(beverage);
                    totalProduced += 1;
                }
                await _batchService.CompleteBatchAsync(batch);
                if (totalProduced >= qty) break;
            }

            if (totalProduced < qty)
            {
                decimal batchesNeeded = (qty - totalProduced) / max;
                var extraBatches = Math.Ceiling(batchesNeeded);
                for (var i = 0; i < extraBatches; i += 1)
                {
                    var batch = new Batch { };
                    await _batchService.CreateBatchAsync(batch);
                    for (var j = 0; j < max; j += 1)
                    {
                        beverage = await _beverageService.Process(beverage);
                        totalProduced += 1;
                    }
                    await _batchService.CompleteBatchAsync(batch);
                }
            }

            // o método fabricate deve consumir de uma fila
            
        }
    }
}