using System;
using System.Threading.Tasks;
using BizCap.Yago.Commons.Enumerateds;
using BizCap.Yago.Domain.Models;
using BizCap.Yago.Domain.Repositories;
using BizCap.Yago.Processor.Processors;

namespace BizCap.Yago.Processor.Services
{
    public class BeverageService
    {
        private readonly IBeverageRepository _repository;
        private readonly BeverageProcessor _processor;

        public BeverageService(IBeverageRepository repository, BeverageProcessor processor)
        {
            _repository = repository;
            _processor = processor;
        }

        public async Task AddBeverage(Beverage beverage)
        {
            await _repository.InsertOneAsync(beverage);
        }

        public async Task<Beverage> GetBeverageByTypeAsync(BeverageTypes type)
        {
            var beverage = await _repository.FindByTypeAsync(type);
            if (beverage == null) throw new Exception("No beverages were found with the required type");
            return beverage;
        }

        public async Task<Beverage> Process(Beverage beverage)
        {
            return await _processor.ProcessSteps(beverage);
        }
    }
}