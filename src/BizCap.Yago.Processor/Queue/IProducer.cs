namespace BizCap.Yago.Processor.Queue
{
    public interface IProducer<T> { 
        void Publish(T queueEvent);
    }
}