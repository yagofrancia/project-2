using System;
using System.Text;
using System.Text.Json;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace BizCap.Yago.Processor.Queue
{
    public abstract class ConsumerBase : RabbitClientBase
    {
        private readonly ILogger<ConsumerBase> _logger;
        public ConsumerBase(ConnectionFactory connectionFactory) : base(connectionFactory)
        {
        }

        public void OnEventReceived<T>(object model, BasicDeliverEventArgs ea)
        {
            try
            {
                var body = Encoding.UTF8.GetString(ea.Body.ToArray());
                var message = JsonSerializer.Serialize(body);
                // chamar o processor para comecar a processar a bebida
            }
            catch (Exception err)
            {
                _logger.LogError(err, "Error while handling event");
            }
        }
    }
}