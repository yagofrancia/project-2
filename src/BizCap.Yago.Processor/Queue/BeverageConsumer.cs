using BizCap.Yago.Processor.Queue.Messages;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace BizCap.Yago.Processor.Queue
{
    public class BeverageConsumer : ConsumerBase
    {
        public BeverageConsumer(ConnectionFactory connectionFactory) : base(connectionFactory)
        {
            var consumer = new EventingBasicConsumer(_channel);
            consumer.Received += OnEventReceived<BeverageRequestMessage>;
            _channel.BasicConsume(queue: QueueName, autoAck: false, consumer: consumer);
        }

        // ver como fazer para colocar mais instancias de consumidores sendo executados
    }
}