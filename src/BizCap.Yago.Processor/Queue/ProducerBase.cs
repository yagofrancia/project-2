using System;
using System.Text;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;

namespace BizCap.Yago.Processor.Queue
{
    public abstract class ProducerBase<T> : RabbitClientBase, IProducer<T>
    {
        private readonly ILogger<T> _logger;

        protected ProducerBase(ConnectionFactory connectionFactory, ILogger<T> logger) : base(connectionFactory)
        {
            _logger = logger;
        }

        public virtual void Publish(T queueEvent)
        {
            try
            {
                var body = Encoding.UTF8.GetBytes(queueEvent.ToString());
                _channel.BasicPublish(exchange: ExchangeName, routingKey: "", body: body);
            }
            catch (Exception err)
            {
                _logger.LogError(err, "Error at message publishing...");
            }
        }
    }
}