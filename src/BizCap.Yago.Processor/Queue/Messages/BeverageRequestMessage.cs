namespace BizCap.Yago.Processor.Queue.Messages
{
    public class BeverageRequestMessage {
        public string Payload { get; set; }
    }
}