using BizCap.Yago.Processor.Queue.Messages;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;

namespace BizCap.Yago.Processor.Queue
{
    public class BeverageRequestProducer : ProducerBase<BeverageRequestMessage>
    {
        public BeverageRequestProducer(ConnectionFactory connectionFactory, ILogger<BeverageRequestMessage> logger) : base(connectionFactory, logger)
        {
        }
    }
}