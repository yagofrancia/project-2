using System;
using RabbitMQ.Client;

namespace BizCap.Yago.Processor.Queue
{
    public abstract class RabbitClientBase : IDisposable
    {
        protected const string ExchangeName = "bizcap-exchange";
        protected const string QueueName = "bizcap-queue";
        private readonly ConnectionFactory _connectionFactory;
        private IConnection _connection;
        protected IModel _channel { get; private set; }

        protected RabbitClientBase(ConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
            Connect();
        }

        private void Connect()
        {
            if (_connection == null || _connection.IsOpen == false)
            {
                _connection = _connectionFactory.CreateConnection();
            }

            if (_channel == null || _channel.IsOpen == false)
            {
                _channel = _connection.CreateModel();
                _channel.ExchangeDeclare(
                    exchange: ExchangeName,
                    type: ExchangeType.Direct
                );
                _channel.QueueDeclare(QueueName);
                _channel.QueueBind(queue: QueueName, exchange: ExchangeName, routingKey: "");
            }
        }

        public void Dispose()
        {
            _channel.Close();
            _channel.Dispose();
            _connection.Close();
            _connection.Dispose();
            _channel = null;
            _connection = null;
        }
    }
}