using BizCap.Yago.Processor.Processors;
using Microsoft.Extensions.DependencyInjection;

namespace BizCap.Yago.Processor.Extensions
{
    public static class ProcessorExtensions
    {
        public static IServiceCollection AddBeverageProcessor(this IServiceCollection service)
        {
            return service.AddScoped<BeverageProcessor>();
        }
    }
}