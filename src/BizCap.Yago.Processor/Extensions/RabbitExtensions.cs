using Microsoft.Extensions.DependencyInjection;
using RabbitMQ.Client;

namespace BizCap.Yago.Processor.Extensions
{
    public static class RabbitExtensions
    {
        public static IServiceCollection AddRabbitConnectionFactory(IServiceCollection service)
        {
            return service.AddSingleton(_ =>
            {
                return new ConnectionFactory { HostName = "localhost" };
            });
        }
    }
}