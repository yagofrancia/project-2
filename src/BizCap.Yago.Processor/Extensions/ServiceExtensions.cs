using BizCap.Yago.Processor.Services;
using Microsoft.Extensions.DependencyInjection;

namespace BizCap.Yago.Processor.Extensions
{
    public static class ServiceExtensions
    {
        public static IServiceCollection AddServices(this IServiceCollection service) =>
            service
                .AddScoped<BeverageService>()
                .AddScoped<IngredientService>();
    }
}