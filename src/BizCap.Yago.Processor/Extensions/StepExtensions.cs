using Microsoft.Extensions.DependencyInjection;
using BizCap.Yago.Processor.Steps;

namespace BizCap.Yago.Processor.Extensions
{
    public static class StepExtensions
    {
        public static IServiceCollection AddProcessingSteps(this IServiceCollection service)
        {
            return service
                        .AddScoped<SeparationStep>()
                        .AddScoped<FiltrationStep>();
        }
    }
}