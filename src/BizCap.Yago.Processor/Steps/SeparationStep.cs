using BizCap.Yago.Commons.Enumerateds;
using BizCap.Yago.Domain.Models;
using BizCap.Yago.Processor.Services;
using System.Threading.Tasks;
namespace BizCap.Yago.Processor.Steps {
    public class SeparationStep : IStep<Beverage>
    {
        private readonly BeverageService _beverageService;
        private readonly IngredientService _ingredientService;

        public SeparationStep(BeverageService beverageService, IngredientService ingredientService)
        {
            _beverageService = beverageService;
            _ingredientService = ingredientService;
        }

        public async Task<Beverage> Process(Beverage entity)
        {
            await _ingredientService.ConsumeIngredient(IngredientTypes.HOP, 1);
            entity.Status = BeverageProcessingStatus.SEPARACAO;
            return entity;
        }
    }
}