using System.Threading.Tasks;

namespace BizCap.Yago.Processor.Steps {
    public interface IStep<T> {
        Task<T> Process(T entity);
    }
}